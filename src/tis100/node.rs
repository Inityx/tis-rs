use std::mem;
use arch::{
    self,
    register::Register,
    instruction::LabelJumpKind,
    instruction::Instruction,
};
use arch::{
    Dest,
    portset::{Input, Output},
    Source,
    Literal,
};
use program::{self, node::NodeProgramConcrete};
use clamp::Clamp;

pub enum TickError { Block, PermaBlock, NoOps }

type TickResult<T> = Result<T, TickError>;

#[derive(Default, Debug)]
pub struct Node {
    current: Option<usize>,
    acc: Register,
    bak: Register,
    last: Option<arch::RealPortId>,
    program: NodeProgramConcrete,
}

impl From<NodeProgramConcrete> for Node {
    fn from(program: NodeProgramConcrete) -> Self {
        Node {
            current: program.first_op_index(),
            program,
            ..Default::default()
        }
    }
}

impl Node {
    /// Returns coupling after execution, Err if blocked on read or all NOP
    pub fn tick(&mut self, inputs: &mut Input) -> TickResult<Output> {
        let current = self.current.ok_or(TickError::NoOps)?;
        let output = self.execute(current, inputs)?;

        self.increment_current();

        Ok(output)
    }

    fn execute(&mut self, current: usize, inputs: &mut Input) -> TickResult<Output> {
        use self::Instruction::*;
        use program::Line;

        let Line(instruction, _, _) = self
            .program
            .lines
            .get(current)
            .expect("Instruction pointer was out of bounds");
        
        match *instruction {
            // Blocking
            Mov(src, dst) => return self.mov(src, dst, inputs),
            Add(source) => self.add(source, inputs)?,
            Sub(source) => self.sub(source, inputs)?,
            Jro(offset) => self.offset_jump(offset, inputs)?,
            // Non-blocking
            Swp => mem::swap(&mut self.acc, &mut self.bak),
            Sav => self.bak = self.acc,
            Neg => self.acc.negate(),
            LabelJump(kind, label_ref) => self.label_jump(kind, label_ref),
            None | Nop => (),
        }

        Ok(Output::None)
    }

    fn mov(&mut self, source: Source, dest: Dest, inputs: &mut Input) -> TickResult<Output> {
        use arch::{Dest, RegId, PortId, portset::Output};

        let value = self.read_from_source(source, inputs)?;

        Ok(match dest {
            Dest::Reg(RegId::Nil) => Output::None,
            Dest::Reg(RegId::Acc) => { self.acc = value.into(); Output::None },
            Dest::Port(id) => match id {
                PortId::Any          => Output::Any(value),
                PortId::RealPort(id) => Output::RealPort(id, value),
                PortId::Last         => Output::RealPort(self.last.ok_or(TickError::PermaBlock)?, value)
            },
        })
    }

    fn add(&mut self, source: Source, inputs: &mut Input) -> TickResult<()> {
        let value = self.read_from_source(source, inputs)?;
        Ok(self.acc += value)
    }

    fn sub(&mut self, source: Source, inputs: &mut Input) -> TickResult<()> {
        let value = self.read_from_source(source, inputs)?;
        Ok(self.acc -= value)
    }

    fn label_jump(&mut self, kind: LabelJumpKind, label_ref: arch::LabelRefIndex) {
        use arch::instruction::LabelJumpKind::*;
        let acc = arch::Word::from(self.acc);

        let jump = match kind {
            Jmp => true,
            Jez => acc == 0,
            Jnz => acc != 0,
            Jgz => acc > 0,
            Jlz => acc < 0,
        };

        if !jump { return }

        self.skip_to_op_from(
            self
                .program
                .labels
                .get(label_ref.into())
                .expect("Indexed nonexistant label")
                .line_index
        );
    }

    fn offset_jump(&mut self, source: Source, inputs: &mut Input) -> TickResult<()> {
        let current = &mut self.current.unwrap();
        let value = self.read_from_source(source, inputs)?;
        *current = (*current + value as usize).clamp(0, arch::NODE_LEN - 1);
        Ok(())
    }
    
    pub fn read_from_source(&mut self, source: Source, inputs: &mut Input) -> TickResult<arch::Word> {
        use arch::{RegId, Source, PortId};

        let value = match source {
            Source::Reg(RegId::Nil) => 0,
            Source::Reg(RegId::Acc) => self.acc.into(),
            Source::Lit(Literal(value))  => value,
            Source::Port(id) => {
                let real_port_id = match id {
                    PortId::RealPort(id) => id,
                    PortId::Last => self.last.ok_or(TickError::PermaBlock)?, // No last port
                    PortId::Any  => {
                        let id = inputs.any().ok_or(TickError::Block)?; // No port with value
                        self.last = Some(id); // Reading from Any sets Last
                        id
                    },
                };

                inputs[real_port_id].take().ok_or(TickError::Block)? // Specified port had no value
            },
        };

        Ok(value)
    }

    fn increment_current(&mut self) {
        if let Some(current) = self.current {
            self.skip_to_op_from(current + 1);
        }
    }

    fn skip_to_op_from(&mut self, mut current: usize) {
        loop {
            match self.program.lines.get(current) {
                None => current = 0,
                Some(program::Line(instruction, _, _)) => {
                    if instruction.is_none() {
                        current += 1
                    } else {
                        break
                    }
                },
            }
        }
        self.current = Some(current);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default() {
        let node = Node::default();
        assert!(node.current.is_none());
        assert_eq!(arch::Word::from(node.acc), 0);
        assert_eq!(arch::Word::from(node.bak), 0);
        assert!(node.last.is_none());
        assert!(node.program.is_empty());
    }
}
