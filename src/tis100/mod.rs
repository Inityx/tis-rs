pub mod node;

use arch::{
    Literal,
    register::Register,
    TIS_WIDTH,
    NUM_NODES,
};
use program::TisProgram;
use self::node::Node;

use arrayvec::ArrayVec;

pub struct InputPort {
    name: String,
    index: usize,
    data: Box<[Literal]>,
}

pub struct OutputPort {
    name: String,
    value: Register,
}

#[derive(Default)]
pub struct Tis100 {
    clock_cycle: usize,
    nodes: [Node; NUM_NODES],
    interconnect: (),
    inputs: ArrayVec<[InputPort; TIS_WIDTH]>,
    outputs: ArrayVec<[OutputPort; TIS_WIDTH]>,
}

impl Tis100 {
    pub fn new(
        program: TisProgram,
        inputs: ArrayVec<[InputPort; TIS_WIDTH]>,
        outputs: ArrayVec<[OutputPort; TIS_WIDTH]>,
    ) -> Self {
        Tis100 {
            nodes: program
                .into_iter()
                .map(Node::from)
                .collect::<ArrayVec<_>>()
                .into_inner()
                .unwrap(),
            inputs,
            outputs,
            ..Default::default()
        }
    }
    
    pub fn tick(&mut self) {

        self.clock_cycle += 1;
    }
}
