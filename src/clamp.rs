use std::ops::RangeInclusive;

pub trait Clamp: Sized + PartialOrd {
    fn clamp(self, start: Self, end: Self) -> Self {
        if self < start { return start }
        if self > end { return end }

        self
    }

    fn clamp_range(self, range: RangeInclusive<Self>) -> Self where Self: Copy {
        if self < *range.start() { return *range.start() }
        if self > *range.end() { return *range.end() }

        self
    }
}

impl<T> Clamp for T where T: PartialOrd {}
