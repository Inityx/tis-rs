use arch::{LabelDef, LabelRefIndex, LabelRefText, NODE_LEN};
use arrayvec::ArrayVec;
use program::{Line, LineAbstract, LineConcrete};
use parse;

#[derive(Default, Debug, PartialEq)]
pub struct LabelDefs(ArrayVec<[LabelDef; NODE_LEN]>);

impl LabelDefs {
    pub fn define(&mut self, line_index: usize, name: impl Into<String>) -> Result<LabelRefIndex, ()> {
        let def = LabelDef { line_index, name: name.into() };
        if self.0.contains(&def) { return Err(()) }
        self.0.push(def);
        Ok(LabelRefIndex(self.0.len() - 1))
    }

    pub fn get(&self, index: usize) -> Option<&LabelDef> {
        self.0.get(index)
    }

    pub fn index_from_text(
        &self,
        LabelRefText(text): LabelRefText,
        line_index: usize,
    ) -> Result<LabelRefIndex, parse::Error>
    {
        self
            .0
            .iter()
            .enumerate()
            .find(|(_, LabelDef { line_index: _, name })| text == *name)
            .map(|(index, _)| LabelRefIndex(index))
            .ok_or(parse::Error { message: "Undefined label", line_index })
    }
}

impl From<ArrayVec<[LabelDef; NODE_LEN]>> for LabelDefs {
    fn from(source: ArrayVec<[LabelDef; NODE_LEN]>) -> Self {
        LabelDefs(source)
    }
}

impl<'a> IntoIterator for LabelDefs {
    type Item = LabelDef;
    type IntoIter = <ArrayVec<[LabelDef; NODE_LEN]> as IntoIterator>::IntoIter;
    fn into_iter(self) -> Self::IntoIter { self.0.into_iter() }
}

#[derive(Default, Debug, PartialEq)]
pub struct NodeProgram<Lines> {
    pub labels: LabelDefs,
    pub lines: Lines,
}

impl<'a, Lines> IntoIterator for &'a NodeProgram<Lines>
where &'a Lines: IntoIterator
{
    type Item = <&'a Lines as IntoIterator>::Item;
    type IntoIter = <&'a Lines as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter { self.lines.into_iter() }
}

impl NodeProgram<[LineConcrete; NODE_LEN]> {
    pub fn first_op_index(&self) -> Option<usize> {
        self
            .into_iter()
            .enumerate()
            .find(|(_, Line(instruction, _, _))| instruction.is_some())
            .map(|(index, _)| index)
    }

    pub fn is_empty(&self) -> bool {
        self.into_iter().all(|Line(instruction, _, _)| instruction.is_none())
    }
}

pub type NodeProgramAbstract = NodeProgram<ArrayVec<[LineAbstract; NODE_LEN]>>;
pub type NodeProgramConcrete = NodeProgram<[LineConcrete; NODE_LEN]>;
