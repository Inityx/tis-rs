pub mod node;

use arrayvec::ArrayVec;
use std::iter::IntoIterator;
use arch::{
    NUM_NODES,
    LabelRefIndex,
    LabelRefText,
    Comment,
    instruction::Instruction,
};
use self::node::NodeProgramConcrete;

#[derive(Default, Debug, PartialEq)]
pub struct Line<LabelRefType>(
    pub Instruction<LabelRefType>,
    pub Option<LabelRefIndex>,
    pub Option<Comment>,
);

pub type LineAbstract = Line<LabelRefText>;
pub type LineConcrete = Line<LabelRefIndex>;

#[derive(Default)]
pub struct TisProgram([NodeProgramConcrete; NUM_NODES]);

impl IntoIterator for TisProgram {
    type Item = NodeProgramConcrete;
    type IntoIter = <ArrayVec<[NodeProgramConcrete; NUM_NODES]> as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        ArrayVec::from(self.0).into_iter()
    }
}
