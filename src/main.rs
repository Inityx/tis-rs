#![feature(nll, transpose_result, range_contains)]
#![allow(dead_code)]

extern crate arrayvec;
extern crate toml;
#[macro_use] extern crate indoc;

mod tis100;
mod arch;
mod program;
mod clamp;
mod parse;

use std::mem::size_of;

fn main() {
    for (name, size) in &[
        ("TIS-100", size_of::<tis100::Tis100>()),
        ("Node", size_of::<tis100::node::Node>()),
        ("NodeProgram", size_of::<program::node::NodeProgramConcrete>()),
        ("Line", size_of::<program::LineConcrete>()),
        ("LabelDefs", size_of::<program::node::LabelDefs>()),
        ("Instruction", size_of::<Option<String>>()),
    ] {
        println!("The size of a {} is {} bytes.", name, size);
    }
}
