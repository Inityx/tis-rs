use std::ops::{AddAssign, SubAssign};
use clamp::Clamp;
use super::{Word, WORD_LIMITS};


#[derive(Debug, Default, Clone, Copy, PartialEq)]
pub struct Register(Word);

impl Register {
    pub fn negate(&mut self) { self.0 *= -1; }
}

impl AddAssign<Word> for Register {
    fn add_assign(&mut self, rhs: Word) {
        self.0 = (self.0 + rhs).clamp_range(WORD_LIMITS);
    }
}

impl SubAssign<Word> for Register {
    fn sub_assign(&mut self, rhs: Word) {
        self.0 = (self.0 - rhs).clamp_range(WORD_LIMITS);
    }
}

impl From<Word> for Register {
    fn from(word: Word) -> Self { Register(word.clamp_range(WORD_LIMITS)) }
}

impl From<Register> for Word {
    fn from(register: Register) -> Self { register.0 }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn creation() {
        assert_eq!(Register( 300), Register::from(  300));
        assert_eq!(Register( -50), Register::from(  -50));
        assert_eq!(Register( 999), Register::from( 1200));
        assert_eq!(Register(-999), Register::from(-1200));
    }

    #[test]
    fn negate() {
        let mut reg = Register(50);
        reg.negate();
        assert_eq!(Register(-50), reg);

        let mut reg = Register(-50);
        reg.negate();
        assert_eq!(Register(50), reg);
    }

    #[test]
    fn add_assign() {
        let mut reg = Register(2);
        reg += 3i16;
        assert_eq!(Register(5), reg);
    }

    #[test]
    fn saturation() {
        let mut reg = Register(900);
        reg += 150;
        assert_eq!(Register(999), reg);

        let mut reg = Register(-900);
        reg -= 150;
        assert_eq!(Register(-999), reg);
    }
}
