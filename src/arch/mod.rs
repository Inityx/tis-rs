pub mod register;
pub mod instruction;
pub mod portset;

use std::ops::RangeInclusive;

pub const WORD_LIMITS: RangeInclusive<Word> = -999..=999;
pub type Word = i16;
pub type Port = Option<Word>;

pub const TIS_WIDTH: usize = 4;
pub const TIS_HEIGHT: usize = 3;
pub const NUM_NODES: usize = TIS_WIDTH * TIS_HEIGHT;
pub const NODE_LEN: usize = 15;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Literal(pub Word);

#[derive(Debug)]
pub struct LabelDef { pub line_index: usize, pub name: String }
impl PartialEq for LabelDef {
    fn eq(&self, rhs: &Self) -> bool {
        self.name == rhs.name 
    }
}

#[derive(Debug, PartialEq)]
pub struct Comment(pub String);

#[derive(Debug, PartialEq)]
pub struct LabelRefText(pub String);

#[derive(Debug, Clone, Copy, PartialEq, Default)]
pub struct LabelRefIndex(pub usize);
impl From<LabelRefIndex> for usize {
    fn from(LabelRefIndex(value): LabelRefIndex) -> Self { value }
}

#[derive(Debug, Clone, Copy, PartialEq)] pub enum RegId { Acc, Nil }
#[derive(Debug, Clone, Copy, PartialEq)] pub enum RealPortId { Up, Right, Down, Left }
#[derive(Debug, Clone, Copy, PartialEq)] pub enum PortId { RealPort(RealPortId), Any, Last } 
#[derive(Debug, Clone, Copy, PartialEq)] pub enum Source { Reg(RegId), Port(PortId), Lit(Literal) }
#[derive(Debug, Clone, Copy, PartialEq)] pub enum Dest { Reg(RegId), Port(PortId) }

impl RealPortId {
    pub fn iter_all() -> impl Iterator<Item=RealPortId> {
        use self::RealPortId::*;
        const PORT_IDS: &[RealPortId] = &[Up, Right, Down, Left];
        PORT_IDS.iter().cloned()
    }
}
