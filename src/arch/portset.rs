use std::ops::{Index, IndexMut};
use arch;

#[derive(Default, Clone, Copy)]
pub struct Input {
    up:    arch::Port,
    right: arch::Port,
    down:  arch::Port,
    left:  arch::Port,
}

impl Index<arch::RealPortId> for Input {
    type Output = arch::Port;

    fn index(&self, idx: arch::RealPortId) -> &Self::Output {
        use arch::RealPortId::*;
        match idx {
            Up    => &self.up,
            Right => &self.right,
            Left  => &self.left,
            Down  => &self.down,
        }
    }
}

impl IndexMut<arch::RealPortId> for Input {
    fn index_mut(&mut self, idx: arch::RealPortId) -> &mut Self::Output {
        use arch::RealPortId::*;
        match idx {
            Up    => &mut self.up,
            Right => &mut self.right,
            Left  => &mut self.left,
            Down  => &mut self.down,
        }
    }
}

impl Input {
    // Return the ID of any real port with data, or None
    pub fn any(&self) -> Option<arch::RealPortId> {
        arch::RealPortId::iter_all().find(|id| self[*id].is_some())
    }
}

#[derive(Clone, Copy)]
pub enum Output {
    None,
    RealPort(arch::RealPortId, arch::Word),
    Any(arch::Word),
}

impl Default for Output {
    fn default() -> Self { Output::None }
}
