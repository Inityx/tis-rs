use super::{Source, Dest, LabelRefIndex, LabelRefText};

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum LabelJumpKind {
    Jmp,
    Jez,
    Jnz,
    Jgz,
    Jlz,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Instruction<LabelRefType> {
    None,
    Nop,
    Mov(Source, Dest),
    Swp,
    Sav,
    Add(Source),
    Sub(Source),
    Neg,
    LabelJump(LabelJumpKind, LabelRefType),
    Jro(Source),
}

impl<LabelRef> Instruction<LabelRef> {
    pub fn is_none(&self) -> bool {
        match self { Instruction::None => true, _ => false }
    }

    pub fn is_some(&self) -> bool {
        !self.is_none()
    }
}

impl<LabelRef> Default for Instruction<LabelRef> {
    fn default() -> Self { Instruction::None }
}

pub type InstructionAbstract = Instruction<LabelRefText>;
pub type InstructionConcrete = Instruction<LabelRefIndex>;
