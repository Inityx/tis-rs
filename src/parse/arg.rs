use std::str::FromStr;
use arch::*;
use super::Error;

impl FromStr for Literal {
    type Err = &'static str;
    fn from_str(token: &str) -> Result<Self, Self::Err> {
        let value: Word = token.parse().map_err(|_| "Invalid argument value")?;

        if WORD_LIMITS.contains(&value) {
            Ok(Literal(value))
        } else {
            Err("Literal out of range")
        }
    }
}

impl FromStr for RegId {
    type Err = ();
    fn from_str(source: &str) -> Result<Self, Self::Err> {
        Ok(match source {
            "ACC" => RegId::Acc,
            "NIL" => RegId::Nil,
            _ => return Err(()),
        })
    }
}

impl FromStr for RealPortId {
    type Err = ();
    fn from_str(source: &str) -> Result<Self, Self::Err> {
        Ok(match source {
            "UP" => RealPortId::Up,
            "RIGHT" => RealPortId::Right,
            "DOWN" => RealPortId::Down,
            "LEFT" => RealPortId::Left,
            _ => return Err(()),
        })
    }
}

impl FromStr for PortId {
    type Err = ();
    fn from_str(source: &str) -> Result<Self, Self::Err> {
        Ok(match source {
            "ANY" => PortId::Any,
            "LAST" => PortId::Last,
            s => return s.parse::<RealPortId>().map(PortId::RealPort),
        })
    }
}

pub(super) trait ParseArg: Sized {
    fn parse_arg<'a>(tokens: &mut impl Iterator<Item=&'a str>, line_index: usize) -> Result<Self, Error> {
        let token = tokens.next().ok_or(Error { message: "Missing required argument", line_index })?;
        Self::from_str(token, line_index)
    }

    fn from_str(&str, usize) -> Result<Self, Error>;
}

impl ParseArg for Source {
    fn from_str(token: &str, line_index: usize) -> Result<Self, Error> {
        token.parse::<RegId>().map(Source::Reg)
            .or_else(|_| token.parse::<PortId>().map(Source::Port))
            .or_else(|_| token.parse::<Literal>().map(Source::Lit).map_err(|s| Error::new(s, line_index)))
    }
}

impl ParseArg for Dest {
    fn from_str(token: &str, line_index: usize) -> Result<Self, Error> {
        token.parse::<RegId>().map(Dest::Reg)
            .or_else(|_| token.parse::<PortId>().map(Dest::Port))
            .or(Err(Error { message: "Invalid argument value", line_index }))
    }
}

impl ParseArg for LabelRefText {
    fn from_str(token: &str, _line_index: usize) -> Result<Self, Error> {
        Ok(LabelRefText(token.into()))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn literal() {
        assert_eq!(Ok(Literal(50)), Literal::from_str("50"));
        assert_eq!(Ok(Literal(-3)), Literal::from_str("-3"));
        assert_eq!(Err("Literal out of range"), Literal::from_str("1000"));
        assert_eq!(Err("Literal out of range"), Literal::from_str("-1000"));
        assert_eq!(Err("Invalid argument value"), Literal::from_str("ASDFHJKL"));
        assert_eq!(Err("Invalid argument value"), Literal::from_str(""));
    }

    #[test]
    fn reg_id() {
        assert_eq!(Ok(RegId::Acc), RegId::from_str("ACC"));
        assert_eq!(Ok(RegId::Nil), RegId::from_str("NIL"));
        assert_eq!(Err(()), RegId::from_str("SDFJHKL"));
        assert_eq!(Err(()), RegId::from_str(""));
    }

    #[test]
    fn real_port_id() {
        assert_eq!(Ok(RealPortId::Up   ), RealPortId::from_str("UP"   ));
        assert_eq!(Ok(RealPortId::Right), RealPortId::from_str("RIGHT"));
        assert_eq!(Ok(RealPortId::Down ), RealPortId::from_str("DOWN" ));
        assert_eq!(Ok(RealPortId::Left ), RealPortId::from_str("LEFT" ));
        assert_eq!(Err(()), RealPortId::from_str("SDFHJKL"));
        assert_eq!(Err(()), RealPortId::from_str(""));
    }

    #[test]
    fn port_id() {
        assert_eq!(Ok(PortId::Any ), PortId::from_str("ANY" ));
        assert_eq!(Ok(PortId::Last), PortId::from_str("LAST"));
        assert_eq!(Ok(PortId::RealPort(RealPortId::Up)), PortId::from_str("UP"));
        assert_eq!(Err(()), PortId::from_str("SDFHJKL"));
        assert_eq!(Err(()), PortId::from_str(""));
    }

    #[test]
    fn source() {
        unimplemented!()
    }

    #[test]
    fn dest() {
        unimplemented!()
    }

    #[test]
    fn label_ref_text() {
        assert_eq!(
            Ok(LabelRefText("ASDFGHJKL".into())),
            LabelRefText::from_str("ASDFGHJKL", 0),
        );
        assert_eq!(
            Ok(LabelRefText(String::new())),
            LabelRefText::from_str("", 0),
        );
    }
}
