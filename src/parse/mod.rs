mod item;
mod arg;
mod file;

use arrayvec::ArrayVec;
use arch::NODE_LEN;
use program::{
    Line,
    LineConcrete,
    node::{
        NodeProgram,
        NodeProgramAbstract,
        NodeProgramConcrete,
        LabelDefs,
    },
};

use arch::instruction::{Instruction, InstructionAbstract, InstructionConcrete};

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Error {
    pub message: &'static str,
    pub line_index: usize,
}

impl Error {
    pub fn new(message: &'static str, line_index: usize) -> Self {
        Error { message, line_index }
    }
}

pub trait Bind: Sized {
    fn bind<T>(self, func: impl FnOnce(Self) -> T) -> T {
        func(self)
    }
}

impl<T> Bind for T {}

pub trait OptionalStr: Sized + AsRef<str> {
    fn into_option<'a>(&'a self) -> Option<&'a str> {
        let string = self.as_ref();
        if string.is_empty() { return None }
        Some(string)
    }
}

impl<T> OptionalStr for T where T: AsRef<str> {}

fn split_trimmer<'a>(delimiter: char) -> impl Fn((&'a str, &'a str)) -> (&'a str, &'a str) {
    move |(first, second)| (first.trim(), second.trim_left_matches(delimiter))
}

pub trait Reify<'a> {
    type Real;
    type Context;
    fn reify(self, Self::Context) -> Result<Self::Real, Error>;
}

impl<'a> Reify<'a> for InstructionAbstract {
    type Real = InstructionConcrete;
    type Context = (&'a LabelDefs, usize);

    fn reify(self, (label_defs, line_index): Self::Context) -> Result<Self::Real, Error> {
        use self::Instruction::*;
        let instruction = match self {
            None      => None,
            Nop       => Nop,
            Mov(s, d) => Mov(s, d),
            Swp       => Swp,
            Sav       => Sav,
            Add(s)    => Add(s),
            Sub(s)    => Sub(s),
            Neg       => Neg,
            Jro(s)    => Jro(s),
            LabelJump(kind, text) => LabelJump(
                kind,
                label_defs.index_from_text(text, line_index)?,
            ),
        };

        Ok(instruction)
    }
}

impl<'a> Reify<'a> for NodeProgramAbstract {
    type Real = NodeProgramConcrete;
    type Context = ();

    fn reify(self, _: Self::Context) -> Result<NodeProgramConcrete, Error> {
        let NodeProgram { labels, lines } = self;

        let lines = ArrayVec::from(lines)
            .into_iter()
            .enumerate()
            .map(|(num, line)| (num + 1, line))
            .try_fold(
                ArrayVec::<[LineConcrete; NODE_LEN]>::new(),
                |mut vec, (line_index, Line(instruction, label_ref, comment))| {
                    vec.push(Line(
                        instruction.reify((&labels, line_index))?,
                        label_ref,
                        comment
                    ));

                    Ok(vec)
                }
            )?
            .into_inner()
            .unwrap();

        Ok(NodeProgramConcrete { labels, lines })
    }
}
