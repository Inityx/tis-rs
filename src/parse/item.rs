use program::{
    Line,
    LineAbstract,
    TisProgram,
    node::{
        LabelDefs,
        NodeProgramAbstract,
        NodeProgram,
    },
};
use parse::{arg::ParseArg, Error, Bind, OptionalStr, split_trimmer};
use arch::{*, instruction::*};
use arrayvec::ArrayVec;

const LABEL_END: char = ':';
const COMMENT_START: char = '#';

pub fn instruction(source: &str, line_index: usize) -> Result<InstructionAbstract, Error> {
    use self::{Instruction::*, LabelJumpKind::*};

    let mut tokens = source.split_whitespace();
    let instruction = if let Some(token) = tokens.next() {
        match token {
            "NOP" => Nop,
            "MOV" => Mov(Source::parse_arg(&mut tokens, line_index)?, Dest::parse_arg(&mut tokens, line_index)?),
            "SWP" => Swp,
            "SAV" => Sav,
            "ADD" => Add(Source::parse_arg(&mut tokens, line_index)?),
            "SUB" => Sub(Source::parse_arg(&mut tokens, line_index)?),
            "NEG" => Neg,
            "JMP" => LabelJump(Jmp, LabelRefText::parse_arg(&mut tokens, line_index)?),
            "JEZ" => LabelJump(Jez, LabelRefText::parse_arg(&mut tokens, line_index)?),
            "JNZ" => LabelJump(Jnz, LabelRefText::parse_arg(&mut tokens, line_index)?),
            "JGZ" => LabelJump(Jgz, LabelRefText::parse_arg(&mut tokens, line_index)?),
            "JLZ" => LabelJump(Jlz, LabelRefText::parse_arg(&mut tokens, line_index)?),
            "JRO" => Jro(Source::parse_arg(&mut tokens, line_index)?),
            _ => return Err(Error { message: "Unknown instruction", line_index }),
        }
    } else {
        None
    };

    if tokens.next().is_some() {
        return Err(Error {
            message: "Unexpected tokens after instruction",
            line_index,
        })
    }

    Ok(instruction)
}

pub trait AppendSourceLine: Sized {
    fn append(self, source_line: &str) -> Result<Self, Error>;
}

impl AppendSourceLine for NodeProgramAbstract {
    fn append(mut self, source_line: &str) -> Result<Self, Error> {
        let (label_text, instruction_text, comment_text) = {
            let (label, rest) = source_line
                .split_at(source_line.find(LABEL_END).unwrap_or(0))
                .bind(split_trimmer(LABEL_END));

            let (instruction, comment) = rest
                .split_at(rest.find(COMMENT_START).unwrap_or_else(||rest.len()))
                .bind(split_trimmer(COMMENT_START));

            (label, instruction, comment)
        };

        let line_index = self.lines.len();

        let label_ref = label_text
            .into_option()
            .map(|label_text| self
                .labels
                .define(self.lines.len(), label_text)
                .map_err(|_| Error { message: "Redefinition of label", line_index })
            )
            .transpose()?;

        let comment = comment_text
            .into_option()
            .map(String::from)
            .map(Comment);

        let instruction = instruction(instruction_text, line_index)?;

        self.lines.push(Line(instruction, label_ref, comment));

        Ok(self)
    }
}

fn node_program(source: &str) -> Result<NodeProgramAbstract, Error> {
    source.lines().try_fold(
        NodeProgram::default(),
        |program, line| program.append(line)
    )
}

fn tis_program(source: &str) -> Result<TisProgram, Error> {
    drop(source);
    unimplemented!()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn instruction_ok() {
        assert_eq!(
            Ok(Instruction::Mov(
                Source::Port(PortId::RealPort(RealPortId::Up)),
                Dest::Reg(RegId::Acc),
            )),
            instruction("MOV UP ACC", 0),
        );
        assert_eq!(
            Ok(Instruction::Jro(Source::Lit(Literal(-2)))),
            instruction("JRO -2", 0),
        );
        assert_eq!(
            Ok(Instruction::Nop),
            instruction("NOP", 0),
        );
        assert_eq!(
            Ok(Instruction::None),
            instruction("", 0),
        );
    }

    fn instruction_err() {
        assert_eq!(
            Err(Error {
                message: "Unexpected tokens after instruction",
                line_index: 5,
            }),
            instruction("MOV UP ACC ACC", 5),
        );
        assert_eq!(
            Err(Error {
                message: "Unknown instruction",
                line_index: 5,
            }),
            instruction("BLARP FOO", 5),
        );
    }

    #[test]
    fn lines() -> Result<(), Error> {
        let parsed = node_program(indoc!("
            LABEL1: JGZ LABEL2  # COMMENT
              LABEL2: MOV UP LAST  # COMMENT2
            LABEL3:
                # JUST COMMENT
            #
            
        "))?;

        let lines = {
            let mut lines = ArrayVec::new();

            lines.push(Line(
                Instruction::LabelJump(LabelJumpKind::Jgz, LabelRefText("LABEL2".into())),
                Some(LabelRefIndex(0)),
                Some(Comment(" COMMENT".into())),
            ));
            lines.push(Line(
                Instruction::Mov(Source::Port(PortId::RealPort(RealPortId::Up)), Dest::Port(PortId::Last)),
                Some(LabelRefIndex(1)),
                Some(Comment(" COMMENT2".into())),
            ));
            lines.push(Line(Instruction::None, Some(LabelRefIndex(2)), None));
            lines.push(Line(Instruction::None, None, Some(Comment(" JUST COMMENT".into()))));
            lines.push(Line(Instruction::None, None, Some(Comment(String::new()))));
            lines.push(Line(Instruction::None, None, None));

            lines
        };

        let labels: LabelDefs = {
            let mut labels = ArrayVec::new();

            labels.push(LabelDef { line_index: 0, name: "LABEL1".into() });
            labels.push(LabelDef { line_index: 1, name: "LABEL2".into() });
            labels.push(LabelDef { line_index: 2, name: "LABEL3".into() });

            labels
        }.into();

        assert_eq!(labels, parsed.labels);
        assert_eq!(lines, parsed.lines);
        Ok(())
    }

    #[test]
    fn line_err() {
        let mut label_defs = LabelDefs::default();
        unimplemented!()
    }
}
