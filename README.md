# TIS-rs

A [TIS-100](http://www.zachtronics.com/tis-100/) emulator in Rust.

## Usage

This emulator primarily supports loading program source files, but could easily
interface with an interactive development environment, as it is in the original
game.

To load a source file, pass its name as the first argument to the program.

## File Format

Emulator programs are written in TOML. Example:

```
Example goes here as soon as I make one lol
```
